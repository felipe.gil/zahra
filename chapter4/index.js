const urlaubsZiel1 = 'Bali 1400CHF Hotels inklusive 4 Sterne frühstuck und Abendessen Tauchen, Surfen, Jetski, Parasailing';
const urlaubsZiel2 = 'Bora Bora 2400CHF Hotels inklusive 5 Sterne frühstuck Tauchen, Jetski, Rochen füttern, Delphinen schwimmen';

//Json Object
const urlaubsAngebot1 = {
  reiseziel: 'Bali',
  uebernachtungsart: 'Hotel',
  uebernachtungsqualitaet: '4 Sterne',
  preis: 1400,
  waehrung: 'CHF',
  essen: {
    fruehstueck: true,
    mittag: false,
    abend: true,
  },
  activitaeten: [
    "Tauchen", 
    "Surfen", 
    "Jetski",
    "Parasailing",
  ],
}

const urlaubsAngebot2 = {
  reiseziel: 'Bora Bora ',
  uebernachtungsart: 'Hotel',
  uebernachtungsqualitaet: '5 Sterne',
  preis: 1399,
  waehrung: 'CHF',
  essen: {
    fruehstueck: true,
    mittag: true,
    abend: true,
  },
  activitaeten: [
    "Tauchen", 
    "Rochen füttern", 
    "Delphinen schwimmen",
  ],
}

//Ausgabe unseres Object
//console.log(urlaubsAngebot1);

//Ausgabe typeof Object
//console.log(typeof urlaubsAngebot1);

//Ausgabe unserer object kette
//console.log(urlaubsAngebot1.reiseziel +' '+ urlaubsAngebot1.preis +''+urlaubsAngebot1.waehrung)

// Get the keys of the object
//const keys = Object.keys(urlaubsAngebot1);

//console.log(keys);
//console.log(urlaubsZiel1);
//console.log(urlaubsZiel2);

//Console log Array activitaeten: [0"Tauchen", 1"Surfen",2"Jetski",3"Parasailing",]
//console.log(urlaubsAngebot1.activitaeten[0])

if(urlaubsAngebot1.preis < urlaubsAngebot2.preis){
  console.log("Urlaub 2 ist Teurer!!!")
}else if(urlaubsAngebot2.activitaeten.length >= urlaubsAngebot1.activitaeten.length){
  console.log("Der Urlaub 2 ist billiger oder gleich Urlaub 1 aber mit mehr Aktivitäten")
}else{
  console.log("Der Urlaub zwei ist Teurer oder Langweiliger als Urlaub 1")
}

switch(urlaubsAngebot2.preis) {
  case 1405:
    // code block
    console.log("Der Urlaub ist mir zu teuer")
    break;
  case 1200:
    // code block
    console.log("Ab in den Urlaub")
    break;
  default:
    // code block
    console.log('wir müssen den Preis im Augebehalten')
}

//Methoden und Functions for next chapter
//Self starting function
function myFunction(parameter) {
  return parameter  // The function returns the product of p1 and p2
}

function urlaubsKosten(urlaub1, urlaub2) {
  return urlaub1 + urlaub2;
}

function urlaubsRabatt(preis, rabat) {
  return preis - ((preis/100) * rabat);
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

console.log(urlaubsKosten(urlaubsAngebot2.preis, urlaubsAngebot2.preis) + 'CHF');
console.log(urlaubsRabatt(urlaubsKosten(urlaubsAngebot2.preis, urlaubsAngebot2.preis), 20)+ 'CHF');
console.log(urlaubsRabatt(urlaubsKosten(urlaubsAngebot2.preis, urlaubsAngebot2.preis), 23.4)+ 'CHF');

console.log(getRandomColor());

