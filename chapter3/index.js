var vorname = 'Zahra';
let nachName = 'Alawade';
const chCourse = 0.91;

//Simple Data Types
let istZahraCool = false;
let inhalt = null;
let name = undefined;
let integer = 10;
let float = 10.3;
let Strings = 'Ich bin ein String'; // '' ""
let EmptyString = '';

name = 'Azavedo'
name = 5


// Alles was hier drunter kommt ist nicht Thema dieser Stunde
//console.info(vorname + " " + nachName);
//console.log(inhalt, name)
//console.log(typeof EmptyString, typeof inhalt);


/*
*
* Operators
*
*/

let x = 5;
const y = 7;
//console.log("This is the value of x:", x);
//console.log("This is the value of y:", y);
const sum = x + y;
const div = x / y;
const modulo = 2400 % 68;
// 7 / 2 =  2 + 2 = 4 + 2 = 6 + 2 > 7 = 1
let increment = ++x;
let sirName = "Zahra";
let lastName = " Alawade";
let FullName = sirName;
FullName += lastName;
// FullName = sirName + lastName;
// FullName = undefint + sirName;

//console.log(sum);
//console.log(div);
//console.log(modulo);
//console.log(FullName);

//Conditional operators
let foo = 5;
let bar = 5;

//equal to
//console.log(foo == bar);
//console.log(foo === bar);

//not equal
//console.log(foo != bar);
//console.log(foo !== bar);

//greater
console.log(foo > bar);
console.log(foo < bar);

//greater then or equal
console.log(foo >= bar);

//ternary operator
// Bedingung ? Ausdruck1(wenn) : Ausdruck2(oder)
let smart = (foo == 7) ? "Zahra is smart" : "Zahra is not smart"
console.log(smart);

//Logical Operators
smart = (foo == 5 && bar == 5) ? "Zahra is smart" : "Zahra is not smart"
console.log(smart);
smart = (foo == 7 || bar == 5) ? "Zahra is smart" : "Zahra is not smart"
console.log(smart);
let muslim = false;
console.log(muslim);
console.log(!muslim);

// declarative Names for Variables
const zahraHandler = () => {
  //..
}
let zahraStressLevel;
let zahraFrisur;
let zahraSpieltWieOfMitIhrenHaaren;
let zahraSpieltMitHaaren;

//Declaration of JS Variables
//Typeof JS Variables
//JS Operators https://www.w3schools.com/js/js_operators.asp
//Declarative Variables Names

//Next Chapter conditional statements
if(foo == 5) {
  return "I am Happy!";
}